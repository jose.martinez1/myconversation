﻿using Microsoft.EntityFrameworkCore;
using MyConversation.Api.Data.Entities;

namespace MyConversation.Api.Data
{
    public class MyDatabaseContext : DbContext
    {
        public MyDatabaseContext(DbContextOptions<MyDatabaseContext> options)
            : base(options) { }

        public DbSet<Conversation>? Conversations { get; set; }
        public DbSet<Message>? Messages { get; set; }
    }
}
