﻿namespace MyConversation.Api.Data.Entities
{
    public class Conversation
    {
        public Conversation()
        {
            Messages = new List<Message>();
        }

        public int Id { get; set; }
        public string? Title { get; set; }
        public ICollection<Message> Messages { get; set; }
    }
}
