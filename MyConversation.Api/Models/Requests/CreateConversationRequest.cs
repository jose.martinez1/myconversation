﻿namespace MyConversation.Api.Models.Requests
{
    public class CreateConversationRequest
    {
        public string? Title { get; set; }
    }
}
